<?php
session_start();
?>
<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>page2</title>
    <link rel="stylesheet" type="text/css" href="index.css">
</head>

<body>
    <form action="" method="post">
        <table border="0px" width="800">
            <?php
            $Quiz = array(
                5 => array(
                    "question" => "a,b,c hay d",
                    "choice" => array(1 => "a", 2 => "b", 3 => "c", 4 => "d"),
                    "answer" => "a"
                ),
                6 => array(
                    "question" => "a,b,c hay d",
                    "choice" => array(1 => "a", 2 => "b", 3 => "c", 4 => "d"),
                    "answer" => "a"
                ),
                7 => array(
                    "question" => "a,b,c hay d",
                    "choice" => array(1 => "a", 2 => "b", 3 => "c", 4 => "d"),
                    "answer" => "a"
                ),
                8 => array(
                    "question" => "a,b,c hay d",
                    "choice" => array(1 => "a", 2 => "b", 3 => "c", 4 => "d"),
                    "answer" => "a"
                ),
                9 => array(
                    "question" => "a,b,c hay d",
                    "choice" => array(1 => "a", 2 => "b", 3 => "c", 4 => "d"),
                    "answer" => "a"
                )
            );
            $size = count($Quiz);
            if (!empty($_POST['submit'])) {
                $mark = $_SESSION['mark'];
                $_SESSION['mark'] = 0;
                for($i = 5; $i < $size+5; $i++){
                    if(!empty($_POST[$i])){
                        $_SESSION['quest'.$i] = $_POST[$i];
                        if($_POST[$i] == $Quiz[$i]['answer']){
                            $mark += 1;
                        }
                    }
                }
                $_SESSION['mark'] = $mark;
                // print_r($_SESSION);
                header("Location: page3.php");
            }
            

            for ($i = 5; $i < 5+$size; $i++) { ?>
                <tr height="40px">
                    <td><?= $Quiz[$i]["question"] ?></td>
                </tr>
                <?php
                for ($j = 1; $j <= 4; $j++) { ?>
                    <tr>
                        <td>
                            <input type="radio" id=<?=$j ?> name=<?= $i ?> value=<?=$Quiz[$i]["choice"][$j]?>>
                            <label for=<?=$j ?>><?= $Quiz[$i]["choice"][$j] ?></label>
                        </td>
                    </tr>
                <?php };
                ?>
            <?php };
            ?>
            <tr height="40px">
                <td align="center"><input class="button" type="submit" name="submit" value="Nộp"></td>
            </tr>
        </table>
    </form>
</body>

</html>
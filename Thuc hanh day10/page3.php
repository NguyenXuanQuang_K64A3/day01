<?php
session_start();
?>
<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="stylesheet" type="text/css" href="index.css">
    <title>Document</title>
</head>

<body>
    <form action="" method="post">
        <table border="0px" width="800">
            <?php
            $Quiz = array(
                0 => array(
                    "question" => "a,b,c hay d",
                    "choice" => array(1 => "a", 2 => "b", 3 => "c", 4 => "d"),
                    "answer" => "a"
                ),
                1 => array(
                    "question" => "a,b,c hay d",
                    "choice" => array(1 => "a", 2 => "b", 3 => "c", 4 => "d"),
                    "answer" => "a"
                ),
                2 => array(
                    "question" => "a,b,c hay d",
                    "choice" => array(1 => "a", 2 => "b", 3 => "c", 4 => "d"),
                    "answer" => "a"
                ),
                3 => array(
                    "question" => "a,b,c hay d",
                    "choice" => array(1 => "a", 2 => "b", 3 => "c", 4 => "d"),
                    "answer" => "a"
                ),
                4 => array(
                    "question" => "a,b,c hay d",
                    "choice" => array(1 => "a", 2 => "b", 3 => "c", 4 => "d"),
                    "answer" => "a"
                ),
                5 => array(
                    "question" => "a,b,c hay d",
                    "choice" => array(1 => "a", 2 => "b", 3 => "c", 4 => "d"),
                    "answer" => "a"
                ),
                6 => array(
                    "question" => "a,b,c hay d",
                    "choice" => array(1 => "a", 2 => "b", 3 => "c", 4 => "d"),
                    "answer" => "a"
                ),
                7 => array(
                    "question" => "a,b,c hay d",
                    "choice" => array(1 => "a", 2 => "b", 3 => "c", 4 => "d"),
                    "answer" => "a"
                ),
                8 => array(
                    "question" => "a,b,c hay d",
                    "choice" => array(1 => "a", 2 => "b", 3 => "c", 4 => "d"),
                    "answer" => "a"
                ),
                9 => array(
                    "question" => "a,b,c hay d",
                    "choice" => array(1 => "a", 2 => "b", 3 => "c", 4 => "d"),
                    "answer" => "a"
                )
            );
            echo "Điểm của bạn là: " . $_SESSION['mark'] . "<br>";
            if ($_SESSION['mark'] < 4) {
                echo "Bạn quá kém cần ôn tập thêm";
            } else if ($_SESSION['mark'] <= 7) {
                echo "Cũng bình thường";
            } else {
                echo "Sắp sửa làm được trợ giảng lớp PHP";
            }
            $size = count($Quiz);
            for ($i = 0; $i < $size; $i++) { ?>
                <tr height="40px">
                    <td><?= $Quiz[$i]["question"] ?></td>
                </tr>
                <?php
                for ($j = 1; $j <= 4; $j++) { ?>
                    <tr>
                        <td>
                            <?php
                            if (!empty($_SESSION['quest' . $i]) and ($_SESSION['quest' . $i] == $Quiz[$i]["choice"][$j])) {
                                if ($_SESSION['quest' . $i] == $Quiz[$i]['answer']) { ?>
                                    <input type="radio" id=<?= $j ?> name=<?= $i ?> value=<?= $Quiz[$i]["choice"][$j] ?> checked>
                                    <label class='true' for=<?= $j ?>><?= $Quiz[$i]["choice"][$j] ?></label>
                                <?php
                                } else { ?>
                                    <input type="radio" id=<?= $j ?> name=<?= $i ?> value=<?= $Quiz[$i]["choice"][$j] ?> checked>
                                    <label class='wrong' for=<?= $j ?>><?= $Quiz[$i]["choice"][$j] ?></label>
                                <?php }
                                ?>
                            <?php
                            } else if ($Quiz[$i]["choice"][$j] == $Quiz[$i]['answer']) { ?>
                                <input type="radio" id=<?= $j ?> name=<?= $i ?> value=<?= $Quiz[$i]["choice"][$j] ?>>
                                <label class='true' for=<?= $j ?>><?= $Quiz[$i]["choice"][$j] ?></label>
                            <?php }else {?>
                                <input type="radio" id=<?= $j ?> name=<?= $i ?> value=<?= $Quiz[$i]["choice"][$j] ?>>
                                <label for=<?= $j ?>><?= $Quiz[$i]["choice"][$j] ?></label>
                            <?php } 
                            ?>
                        </td>
                    </tr>
                <?php };
                ?>
            <?php };
            session_unset();
            ?>
        </table>
    </form>
</body>

</html>
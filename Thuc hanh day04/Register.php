<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Day04</title>
    <link rel="stylesheet" type="text/css" href="./index.css">
</head>

<body>
    <!--<?php
        // $name = $email = $gender = $comment = $website = "";

        // if ($_SERVER["REQUEST_METHOD"] == "POST") {
        //     function test_input($data)
        //     {
        //         $data = trim($data);
        //         $data = stripslashes($data);
        //         $data = htmlspecialchars($data);
        //         return $data;
        //     }
        //     $name = test_input($_POST["name"]);
        //     $gender = test_input($_POST["gender"]);
        //     $depart = test_input($_POST["depart"]);
        //     $birthday = test_input($_POST["birthday"]);
        // }
        ?> -->

    <form action="Register.php" method="post">
        <table border="0px" width="400">
            <?php
            if (isset($_POST['submit'])) {


                if (empty(test_input($_POST["name"]))) {
                    echo ("<tr height=\"20px\">
                        <td class=\"error\">Hãy nhập tên</td>
                    </tr>");
                }
                if (empty($_POST["gender"])) {
                    echo ("<tr height=\"20px\">
                        <td class=\"error\">Hãy chọn giới tính</td>
                    </tr>");
                }
                if (empty($_POST["departs"])) {
                    echo ("<tr height=\"20px\">
                        <td class=\"error\">Hãy chọn phân khoa</td>
                    </tr>");
                }
                if (empty(test_input($_POST["birthday"]))) {
                    echo ("<tr height=\"20px\">
                        <td class=\"error\">Hãy nhập ngày sinh</td>
                    </tr>");
                } else {
                    $birthday = test_input($_POST["birthday"]);
                    
                    if (!preg_match("/(^(((0[1-9]|1[0-9]|2[0-8])[\/](0[1-9]|1[012]))|((29|30|31)[\/](0[13578]|1[02]))|((29|30)[\/](0[4,6,9]|11)))[\/](19|[2-9][0-9])\d\d$)|(^29[\/]02[\/](19|[2-9][0-9])(00|04|08|12|16|20|24|28|32|36|40|44|48|52|56|60|64|68|72|76|80|84|88|92|96)$)/", $birthday)) {
                        echo ("<tr height=\"20px\">
                        <td class=\"error\">Hãy nhập ngày sinh đúng định dạng ngày sinh</td>
                    </tr>");
                    }
                }
            }
            function test_input($data)
            {
                $data = trim($data);
                $data = stripslashes($data);
                $data = htmlspecialchars($data);
                return $data;
            }
            ?>
            <tr height="40px">
                <td class="label">Họ và tên <sup class="star">*</sup></td>
                <td class="space"></td>
                <td class="input"><input type="text" name="name"></td>
            </tr>
            <tr class="space"></tr>
            <tr height="40px">
                <td class="label name">Giới tính <sup class="star">*</sup></td>
                <td class="space"></td>
                <td class="checkBox"><?php
                                        $Gender = array(0 => "Nam", 1 => "Nữ");
                                        for ($i = 0; $i < 2; $i++) { ?>
                        <input type="radio" id=<?= $i ?> name="gender">
                        <label for="<?= $i ?>"><?= $Gender[$i] ?></label>
                    <?php }
                    ?>
                </td>
            </tr>
            <tr class="space"></tr>
            <tr height="40px">
                <td class="label name">Phân Khoa <sup class="star">*</sup></td>
                <td class="space"></td>
                <td class="selectBox">
                    <select name="departs" id="depart">
                        <option value="none" selected disabled hidden></option>
                        <?php
                        $depart = array("MAT" => "Khoa học máy tính", "KDL" => "Khoa học vật liệu");
                        foreach ($depart as $key => $value) { ?>
                            <option value="<?= $key ?>"><?= $value ?></option>
                        <?php };
                        ?>
                    </select>
                </td>
            </tr>
            <tr class="space"></tr>
            <tr height="40px">
                <td class="label name">Ngày sinh <sup class="star">*</sup></td>
                <td class="space"></td>
                <td class="input"><input type="text" name="birthday" pattern="[0-9]{2}/[0-9]{2}/[0-9]{4}" placeholder="dd/mm/yyyy" title="Enter a date in this format dd/mm/yyyy" /></td>
            </tr>
            <tr class="space"></tr>
            <tr height="40px">
                <td class="label name">Địa chỉ</td>
                <td class="space"></td>
                <td class="input"><input type="text" name="address"></td>
            </tr>
            <tr height="80px">
                <td colspan="5" rowspan="5" align="center"><input class="button" type="submit" name="submit" value="Đăng ký"></td>
            </tr>
        </table>
    </form>
</body>

</html>
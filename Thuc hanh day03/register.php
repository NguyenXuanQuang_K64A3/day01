<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <title>Day03</title>
    <link rel="stylesheet" href="index.css">
</head>

<body>
    <form action="register.php">
        <table border="0px" width="400">
            <tr height="40px">
                <td class="label name">Họ và tến</td>
                <td class="space"></td>
                <td class="input" require><input type="text" name="name"></td>
            </tr>
            <tr class="space"></tr>
            <tr height="40px">
                <td class="label name">Giới tính</td>
                <td class="space"></td>
                <td class="checkBox"><?php
                                    $Gender = array(0 => "Nam", 1 => "Nữ");
                                    for ($i = 0; $i < 2; $i++) { ?>
                        <input type="radio" id=<?= $i ?> name="gender">
                        <label for="<?= $i ?>"><?= $Gender[$i] ?></label>
                    <?php }
                    ?>
                </td>
            </tr>
            <tr class="space"></tr>
            <tr height="40px">
                <td class="label name">Phân Khoa</td>
                <td class="space"></td>
                <td class="selectBox">
                    <select name="depart" id="depart" >
                    <option value="none" selected disabled hidden></option>
                        <?php
                        $depart = array("MAT" => "Khoa học máy tính", "KDL" => "Khoa học vật liệu");
                        foreach ($depart as $key => $value) { ?>
                            <option value="<?=$key?>"><?= $value ?></option>
                        <?php };
                        ?>
                    </select></td>
            </tr>
            <tr height="80px">
                <td colspan="5" rowspan="5" align="center"><input class="button" type="submit" name="submit" value="Đăng ký"></td>
            </tr>
        </table>
    </form>
</body>

</html>
<?php
session_start();
?>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="stylesheet" type="text/css" href="index.css">
    <title>Document</title>
</head>
<body>
    <?php
    echo "Điểm của bạn là: " . $_SESSION['mark'] . "<br>";
    if ($_SESSION['mark'] < 4){
        echo "Bạn quá kém cần ôn tập thêm";
    }else if($_SESSION['mark'] <= 7){
        echo "Cũng bình thường";
    }else{
        echo "Sắp sửa làm được trợ giảng lớp PHP";
    }
    ?>
</body>
</html>